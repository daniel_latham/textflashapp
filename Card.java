/*
 * Card.java
 * 
 * Copyright 2014 Daniel Latham <l3li3l@torguard.tg>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
public final class Card {
	///Front of card, can be empty string
	public final String front;
	///Back of card, can be empty string
	public final String back;
	/** Constructor method, just sets the front and
	 * back of this card.
	 * @param String Front of the card you want to initialize
	 * @param String Back of the card you want ot initialize
	 */
	public Card(String frontside, String backside)
	{
		front = frontside;
		back = backside;
	}
}

