/*
 * FlashCardApp.java
 * 
 * Copyright 2014 Daniel Latham <l3li3l@torguard.tg>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
/** Main class of my flashcard application. Utilizes several classes including
 * FlashSet, Leitner, Pool, and Card
 *  
 */
import java.io.Console;
import java.util.regex.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class FlashCardApp
{
	private static void study(FlashSet currentSet)
	{
		try
		{
			boolean aPool = currentSet.getLength(0) >= 1;
			boolean bPool = currentSet.getLength(1) >= 1;
			boolean cPool = currentSet.getLength(2) >= 1;
			boolean dPool = currentSet.getLength(3) >= 1;
			boolean fPool = currentSet.getLength(4) >= 1;
			if (currentSet != null && (aPool || bPool || cPool || dPool || fPool))
			{
				Leitner.study(currentSet);
			}
		}
		catch (Exception e)
		{
			System.out.println(e+"\nNo set detected. Returning to main menu...");
			//System.out.println(e);//"You haven't initialized a new set yet! Type \"!newset\" or \"!import\" instead.");
		}
	}
	private static void list(FlashSet currentSet)
	{
		 ///Checks to make sure current Flash Set is not null, procedes
		if (currentSet != null)
		{
			Console console = System.console();
			String input = console.readLine("Number 1-5 inclusive or string you want to search for:");
			Pattern empty = Pattern.compile("");
			Matcher emptyMatcher = empty.matcher(input);
			if (!emptyMatcher.matches())
				try
				{ ///Trys to convert input to integer, and if fails 
					///it means that user is trying to list all occurances of a String
					int intInput = Integer.parseInt(input); 
					currentSet.list(intInput);
				}
				catch (Exception e)
				{
					Pattern newPattern = Pattern.compile("(.*?)"+input+"(.*?)");
					currentSet.list(newPattern);
				}
			else
			{
				System.out.println("Empty string input detected. Returning to main menu...");
			}
		}
		else
		{
			System.out.println("No set detected. Returning to main menu...");
		}
	}
	private static void exit(FlashSet currentSet)
	{
		//clearScreen();
		if (currentSet != null)
		{
			currentSet.saveSet();
		}
		System.out.println("Goodbye!");
		try {Thread.sleep(1500);}
		catch (Exception e) {}
		System.exit(0);
	}
	/** Method to clear terminal screen but is currently not working.
	 * @param void
	 * @throws File read error
	 * @return void
	 */
	private static void clearScreen()
	{
		try
		{
			String command = "ls -c";
			
			Process proc = Runtime.getRuntime().exec(command);
				
			BufferedReader reader = 
				new BufferedReader(new InputStreamReader(proc.getInputStream()));
			
			String line = "";
			while((line = reader.readLine()) != null) {
				System.out.print(line + "\n");
			}
			proc.waitFor();   
		} catch (Exception e) {}
	}
	/** Displays help messages in/for the main menu. 
	 * @param void
	 * @return void
	 */
	private static void help()
	{
		System.out.println("Commands are as follows: !help, !newset, !import, !list, !listall, !study, and !exit");
	}
	/** Displays(prints to System.out) the greeting.
	 * @param void
	 * @return void
	 */
	private static void greeting()
	{
		System.out.println("|********************************************************|");
		System.out.println("**********************************************************");
		System.out.println("|||||||||||||||||||||| WELCOME |||||||||||||||||||||||||||");
		System.out.println();
		System.out.println("You are using GNU free software. Please visit the FSF for\ninformation related to licensing.");
		System.out.println();
		System.out.println();
	}
	/** Main method of my application that calls on Leitner and FlashSet.
	 * <dt><b>Precondition:</b><dd>
	 * Must run with no arguments. All arguments will be ignored.
	 * @param String[] Arguments from commandline
	 * @return void
	 */
	public static void main (String args[]) 
	{
		greeting();
		FlashSet currentSet = null;
		Console console = System.console();
		String input = console.readLine("Press the enter key to continue");
		///Attempts to read SET.txt under working directory
		try
		{
			currentSet = new FlashSet("SET.txt");
		} catch (Exception e)
		{
			System.out.println("SET.txt not found. Either this is your first run, or you have deleted the file.");
		}
		System.out.println("What would you like to do now? Enter \"!help\" for options");
		/// Patterns I want to catch
		Pattern exit = Pattern.compile("!exit");
		Pattern help = Pattern.compile("!help");
		Pattern newset = Pattern.compile("!newset");
		Pattern study = Pattern.compile("!study");
		Pattern importp = Pattern.compile("!import");
		Pattern listall = Pattern.compile("!list");
		Pattern listAll = Pattern.compile("!listall");
		boolean offswitch = false;
		/// Loops until input == !exit
		while (!offswitch)
		{
			input = console.readLine(":");
			/// Matchers for all Patterns
			Matcher helpMatcher = help.matcher(input);
			Matcher exitMatcher = exit.matcher(input);
			Matcher newsetMatcher = newset.matcher(input);
			Matcher studyMatcher = study.matcher(input);
			Matcher importMatcher = importp.matcher(input);
			Matcher listMatcher = listall.matcher(input);
			Matcher listAllMatcher = listAll.matcher(input);
			/// Start if if-else blocks
			if (helpMatcher.matches())
			{
				help();
			}
			else if (exitMatcher.matches())
			{
				exit(currentSet);
			}
			else if (newsetMatcher.matches())
			{
				//clearScreen();
				currentSet = new FlashSet();
				if (currentSet.getLength(0) != 0)
				{
					System.out.println("Your new FlashCard Set has been created with the values you entered!");
					System.out.println("Type \"!study\" to study with the Leitner system!");
				}
				else
				{
					System.out.println("You didn't create any cards in your new set! Please try again!");
				}
			}
			else if (importMatcher.matches())
			{
				String filename = console.readLine("Filename path?:");
				currentSet = new FlashSet(filename);
			}
			else if (listMatcher.matches())
			{
				list(currentSet);
			}
			else if (listAllMatcher.matches())
			{ ///Lists all created cards
				if (currentSet != null)
				{
					currentSet.listAll();
				}
				else
				{
					System.out.println("No set detected. Returning to main menu...");
				}
			}
			else if (studyMatcher.matches())
			{ ///Calls on Leitner to go into study mode if current set is initialized and has cards.
				study(currentSet);
			}
			else
			{
				System.out.println("I didn't quite catch that. Type \"!help\" for commands");
			}
		}
		
	}
}

