/*
 * FlashSet.java
 * 
 * Copyright 2014 Daniel Latham <l3li3l@torguard.tg>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
import java.io.File;
import java.util.regex.*;
import java.util.Scanner;
import java.io.PrintWriter;
public class FlashSet 
{
	public Pool[] pools;
	public Pool pool1,pool2,pool3,pool4,pool5;
	/** Saves current set in working directory w/ pools saved as well
	 */
	public int getLength(int whichPool)
	{
		return pools[whichPool].getLength();
	}
	public void saveSet()
	{
		PrintWriter kWriter;
		try
		{
			kWriter = new PrintWriter("SET.txt", "UTF-8");
		}
		catch (Exception e)
		{
			kWriter = null;
		}
		for (int i = 0; i < pools.length; i++)
		{
			if (pools[i].getLength() >= 1)
			{
				for (int j = pools[i].getLength() -1; j >= 0; j--)
				{
					Card temp = pools[i].removeCard(j);
					kWriter.println(i);
					kWriter.println(temp.front);
					kWriter.println(temp.back);
				}
			}
			else
			{
				//Nothing
			}
		}
		kWriter.close();
		System.out.println("File saved as \'SET.txt\' under working directory!");
	}
	public void moveCard(int poolInitNum, int poolFinalNum, int cardNum)
	{
		try
		{
			if (poolFinalNum <= 5)
			{
				Card returned = pools[poolInitNum-1].removeCard(cardNum);
				pools[poolFinalNum-1].addCard(returned);
			}
			///Does nothing if bigger than 5 because pool is already at #5 which is the highest one
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}
	public void list(Pattern newPattern)
	{
		Matcher fMatcher;
		Matcher bMatcher;
		for (int i = 0; i <= 4; i++)
		{
			for (int j = 0; j <= pools[i].getLength()-1; j++)
			{
				fMatcher = newPattern.matcher(pools[i].getC(j, true));
				bMatcher = newPattern.matcher(pools[i].getC(j, false));
				if(fMatcher.find() || bMatcher.find())
				{
					System.out.println();
					System.out.println("Input found in...");
					System.out.println("Pool #"+(i+1)+" Card #"+(j+1));
					System.out.println("Front: " + pools[i].getC(j, true));
					System.out.println("Back: " + pools[i].getC(j, false));
					System.out.println();
				}
			}
		}
	}
	public void listAll()
	{
		for (int i = 1; i <= 5; i++)
		{
			list(i);
		}
	}
	public void list(int whichPool)
	{
		int len = pools[whichPool-1].getLength()-1;
		if (len == -1)
		{
			System.out.println("No cards present in Pool #"+whichPool);
		}
		for (int i = 0; i <= len; i++)
		{
			String front = pools[whichPool-1].getC(i, true);
			String back = pools[whichPool-1].getC(i, false);
			System.out.println();
			System.out.println("Pool #"+whichPool+" Card #"+(i+1));
			System.out.println("Front: " + front);
			System.out.println("Back: " + back);
			System.out.println();
		}
	}
	public int pickPool()
	{
		//Need to make this so that pool1 is twice as likely as pool2, and so on
		int whichpool = 0;
		boolean stopswitch = false;
		boolean initPools1 = pool1.getLength() >= 1;
		boolean initPools2 = pool2.getLength() >= 1;
		boolean initPools3 = pool3.getLength() >= 1;
		boolean initPools4 = pool4.getLength() >= 1;
		boolean initPools5 = pool5.getLength() >= 1;
		while (!stopswitch)
		{
			double which = Math.random()*32;
			if (which >= 16 && initPools1)
			{
				whichpool = 1;
				stopswitch = true;
			}
			else if (which >= 8 && initPools2)
			{
				whichpool = 2;
				stopswitch = true;
			}
			else if (which >= 4 && initPools3)
			{
				whichpool = 3;
				stopswitch = true;
			}
			else if (which >= 2 && initPools4)
			{
				whichpool = 4;
				stopswitch = true;
			}
			else if (which >= 1 && initPools5)
			{
				whichpool = 5;
				stopswitch = true;
			}
		}
		return whichpool;
	}
	public void createPools(boolean oneOff)
	{
		pool1 = new Pool(oneOff);
		pool2 = new Pool(false);
		pool3 = new Pool(false);
		pool4 = new Pool(false);
		pool5 = new Pool(false);
		pools = new Pool[5];// {pool1, pool2, pool3, pool4, pool5};
		
		pools[0] = pool1;
		pools[1] = pool2;
		pools[2] = pool3;
		pools[3] = pool4;
		pools[4] = pool5;
	}
	public FlashSet(String filename)
	{
		System.out.println("Importing file " + filename + "...");
		try
		{
			createPools(false);
			File file = new File(filename);
			if (file.exists())
			{
				try
				{
					Scanner scanner = new Scanner(file);
					while (scanner.hasNextLine())
					{
						//iterate forward once more
						String num = scanner.nextLine(); //CAN BE null(""), and if so, goes to pool 1
						String front = scanner.nextLine();
						String back = scanner.nextLine();
						int i = 0;
						try 
						{
							i = Integer.parseInt(num);
						} catch (Exception e)
						{
							//Nothing
						}
						Card instance = new Card(front,back);
						pools[i].addCard(instance);
					}
				}
				catch (Exception e)
				{
					System.out.println(e);
				}
				Thread.sleep(20);
			}
			else
			{
				Exception e = new Exception("File not found!");
				throw e;
			}
		}
		catch (Exception e) {System.out.println("File not found!");}
	}
	public FlashSet()
	{
		System.out.println("*************NEW FLASH SET CREATOR MODE**************");
		createPools(true);
		if (pool1.getLength() == 0)
		{
			///Nothing on purpose I think but I'll have to check later
		}
		else
		{
			System.out.println("Pools created successfully!");
		}
	}
}

