/*
 * Leitner.java
 * 
 * Copyright 2014 Daniel Latham <l3li3l@torguard.tg>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
import java.io.Console;
import java.util.regex.*;
public class Leitner 
{
	public static void disHelp()
	{
		System.out.println("*****************STUDY MODE************************");
		System.out.println("CONTROLS:\nIf you know the flipside, hit enter,\n or type in the string that you think it is plus enter.");
		System.out.println("\"%\": Flips card so you can see the back/front");
		System.out.println("\"!exit\": Stops study session.");
		System.out.println("\"!help\": Displays these options again.");
	}
	public static void study(FlashSet currentSet)
	{
		disHelp();
		Console console = System.console();
		boolean stopswitch = false;
		Pattern stoppattern = Pattern.compile("!exit");
		Pattern helppattern = Pattern.compile("!help");
		Pattern a = Pattern.compile("%");
		Pattern nullpattern = Pattern.compile("");
		while(!stopswitch)
		{
			int poolNum = currentSet.pickPool();
			String[] strArray = currentSet.pools[poolNum-1].pickCard();
			String front = strArray[0];
			String back = strArray[1];
			Pattern frontpattern = Pattern.compile(front);
			Pattern backpattern = Pattern.compile(back);
			int indexNum = Integer.parseInt(strArray[2]);
			boolean sideswitch;
			if (Math.random() > .5)
			{
				sideswitch = true; //frontside
				System.out.println(front);
			}
			else
			{
				sideswitch = false;//backside
				System.out.println(back);
			}
			String input = console.readLine(":");
			Matcher stopmatcher = stoppattern.matcher(input);
			Matcher helpmatcher = helppattern.matcher(input);
			Matcher amatcher = a.matcher(input);
			Matcher frontmatcher = frontpattern.matcher(input);
			Matcher backmatcher = backpattern.matcher(input);
			Matcher nullmatcher = nullpattern.matcher(input);
			if (helpmatcher.matches())
			{
				while (helpmatcher.matches())
				{
					disHelp();
					input = console.readLine(":");
					stopmatcher = stoppattern.matcher(input);
					helpmatcher = helppattern.matcher(input);
					amatcher = a.matcher(input);
					frontmatcher = frontpattern.matcher(input);
					backmatcher = backpattern.matcher(input);
					nullmatcher = nullpattern.matcher(input);
				}
			}
			if (amatcher.matches())
			{
				while(amatcher.matches())
				{
					if (sideswitch == true)
					{
						System.out.println("Showing back of card...");
						System.out.println(back);
						sideswitch = false;
						input = console.readLine(":");
						amatcher = a.matcher(input);
					}
					else
					{
						System.out.println("Showing front of card...");
						System.out.println(front);
						sideswitch = true;
						input = console.readLine(":");
						amatcher = a.matcher(input);
					}
				}
			}
			if (stopmatcher.matches())
			{
				stopswitch = true;
			}
			else if (frontmatcher.matches() && sideswitch == false)
			{
				currentSet.moveCard(poolNum, poolNum+1, indexNum);
				System.out.println("Correct!");
				try
				{
					Thread.sleep(2000);
				}
				catch (Exception e)
				{
				}
				///Bazillion newlines so that you have to scroll up to see the last card
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
			}
			else if (backmatcher.matches() && sideswitch == true)
			{
				currentSet.moveCard(poolNum, poolNum+1, indexNum);
				System.out.println("Correct!");
				try
				{
					Thread.sleep(2000);
				}
				catch (Exception e)
				{
				}
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
			}
			else if (nullmatcher.matches())
			{
				//this will signify the dude knows the word
				currentSet.moveCard(poolNum, poolNum+1, indexNum);
				System.out.println("Good one! Card has been moved to a higher pool.");
				try
				{
					Thread.sleep(2000);
				}
				catch (Exception e)
				{
				}
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
			}
			else
			{
				System.out.println("Sorry, maybe next time!");
				try
				{
					Thread.sleep(2000);
				}
				catch (Exception e)
				{
				}
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
			}
		}
		System.out.print("Congratulations you finished the study lesson!\nYou are being moved back to the main menu... ");
		System.out.println();
	}
}

