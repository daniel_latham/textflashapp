/*
 * Pool.java
 * 
 * Copyright 2014 Daniel Latham <l3li3l@torguard.tg>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
import java.io.Console;
import java.io.File;
import java.util.regex.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Pool {
	private ArrayList<Card> cards = new ArrayList<Card>();
	public String getC(int i, boolean fb)
	{
		if (fb)
		{
			return cards.get(i).front;
		}
		else
		{
			return cards.get(i).back;
		}
	}
	public String[] pickCard()
	{
		Double Dwhichcard = Math.random()*cards.size();
		int whichcard = Dwhichcard.intValue();
		String front = cards.get(whichcard).front;
		String back = cards.get(whichcard).back;
		return new String[]{front,back,Integer.toString(whichcard)};
	}
	public void addCard(Card newCard)
	{
		cards.add(newCard);
	}
	public Card removeCard(int cardNum)
	{
		Card RnewCard = cards.remove(cardNum);
		cards.trimToSize();
		return RnewCard;
	}
	public int getLength()
	{
		return cards.size();
	}
	public void createCardsManual()
	{
		Console console = System.console();
		System.out.println("Type \"!exit\" to stop generating new cards.");
		boolean stopswitch = false;
		long cnumber = 1;
		while(!stopswitch)
		{
			System.out.println();
			System.out.println("Card#"+cnumber);
			Pattern stoppattern = Pattern.compile("!exit");
			String front = console.readLine("What text you want on the Front side this of card?\n:");
			String back = console.readLine("What text do you want on the Back side of this card?\n:");
			Matcher frontstopmatcher = stoppattern.matcher(front);
			Matcher backstopmatcher = stoppattern.matcher(back);
			Card instance = new Card(front, back);
			if (frontstopmatcher.matches() || backstopmatcher.matches())
			{
				stopswitch = true;
				System.out.println("Stopswitch activated. No longer in create cards mode.");
				System.out.println(cards.size() + " cards initialized.");
				try
				{
					Thread.sleep(1000);
				}
				catch(Exception e){}
			}
			else
			{
				cards.add(instance);
			}
			cnumber += 1;
		}
		//initializes/adds cards to array
	}
	
	public Pool(boolean initialize)
	{
		if (initialize)
		{
			createCardsManual();
		}
	}
}

